# Manage Weblate dependencies that are in not in Debian
#
# **WARNING**
# This file was generated automatically, do not manually modify it.
#
class tails::weblate::python_modules () {

  $$pip_packages = [
    'python3-pip',
    'python3-setuptools',
  ]

  ensure_packages($$pip_packages)

  # Dependencies for Weblate $WEBLATE_VERSION

  $PIP_DEPENDS
}
