# Manage log directories and rotation
class tails::weblate::logs(
  Stdlib::Absolutepath $mutable_data_dir,
  Stdlib::Absolutepath $logs_dir,
) {

  file { $logs_dir:
    ensure => directory,
    owner  => root,
    group  => weblate,
    mode   => '2770',
  }

  file { "${mutable_data_dir}/logs":
    ensure => link,
    target => $logs_dir,
  }

  file { '/etc/logrotate.d/weblate':
    source  => 'puppet:///modules/tails/weblate/logrotate/logrotate.conf',
    owner   => root,
    group   => root,
    mode    => '0644',
    require => File[$logs_dir],
  }

}

