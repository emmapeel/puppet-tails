# Manage Weblate dependencies that are in Debian
#
# **WARNING**
# This file was generated automatically, do not manually modify it.
#
class tails::weblate::debian_packages () {

  $packages = [
    'ccze', 'ipython3',  # for more convenient debugging
    'mercurial',  # so we can use pip to install from hg repos
    # Dependencies for Weblate 3.5.1
    'python3-chardet',
    'python3-cryptography',
    'python3-dateutil',
    'python3-defusedxml',
    'python3-diff-match-patch',
    'python3-jwt',
    'python3-levenshtein',
    'python3-lxml',
    'python3-memcache',
    'python3-mysqldb',
    'python3-openid',
    'python3-pil',
    'python3-pyuca',
    'python3-rcssmin',
    'python3-requests',
    'python3-requests-oauthlib',
    'python3-ruamel.yaml',
    'python3-sqlparse',
    'python3-tz',
    'python3-whoosh',
    'python3-yaml',
    'sqlite3',
  ]

  ensure_packages($packages)

  # The following file is a slightly patched version of the original one to
  # address a memory leak that spits out unharmful (but annoying) exceptions
  # every once in a while. We need that to avoid noise in our custom Python
  # scripts.
  #
  # TODO: remove once the VM is upgraded to a newer version of Debian.

  ensure_packages(['libpython3.5-minimal'])

  file { '/usr/lib/python3.5/weakref.py':
    source  => 'puppet:///modules/tails/weblate/python/weakref.py',
    mode    => '0644',
    owner   => root,
    group   => root,
    require => Package['libpython3.5-minimal'],
  }

}
