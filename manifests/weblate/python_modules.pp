# Manage Weblate dependencies that are in not in Debian
#
# **WARNING**
# This file was generated automatically, do not manually modify it.
#
class tails::weblate::python_modules () {

  $pip_packages = [
    'python3-pip',
    'python3-setuptools',
  ]

  ensure_packages($pip_packages)

  # Dependencies for Weblate 3.5.1

  tails::pip_package_from_repo { 'amqp':
    version => '2.6.0',  # >=2.6.0, <2.7
    url     => 'http://github.com/celery/py-amqp',
    require => [Exec['pip_install_vine']],
  }

  tails::pip_package_from_repo { 'asgiref':
    version => '3.2.8',  # ~=3.2
    url     => 'https://github.com/django/asgiref/',
  }

  tails::pip_package_from_repo { 'backports.functools-lru-cache':
    version => '1.6.1',
    tag     => 'v1.6.1',
    url     => 'https://github.com/jaraco/backports.functools_lru_cache',
  }

  tails::pip_package_from_repo { 'billiard':
    version => '3.6.3.0',  # >=3.6.3.0, <4.0
    url     => 'https://github.com/celery/billiard',
  }

  tails::pip_package_from_repo { 'celery':
    version => '4.4.5',  # >=4.0
    tag     => 'v4.4.5',
    url     => 'https://github.com/celery/celery',
    require => [Exec['pip_install_billiard'], Exec['pip_install_future'], Exec['pip_install_kombu'], Exec['pip_install_redis'], Exec['pip_install_vine']],
  }

  tails::pip_package_from_repo { 'celery-batches':
    version => '0.3',  # >=0.2
    tag     => 'v0.3',
    url     => 'https://github.com/percipient/celery-batches',
    require => [Exec['pip_install_celery']],
  }

  tails::pip_package_from_repo { 'cheroot':
    version => '8.3.0',
    tag     => 'v8.3.0',
    url     => 'https://github.com/cherrypy/cheroot',
    require => [Exec['pip_install_backports.functools-lru-cache'], Exec['pip_install_jaraco.functools'], Exec['pip_install_more-itertools'], Exec['pip_install_six']],
  }
  tails::pip_package_from_repo { 'django':
    version => '1.11.29',  # >=1.11, <2.0
    url     => 'https://github.com/django/django',
    require => [Exec['pip_install_asgiref']],
  }

  tails::pip_package_from_repo { 'django-appconf':
    version => '1.0.4',  # >=1.0
    tag     => 'v1.0.4',
    url     => 'https://github.com/django-compressor/django-appconf',
    require => [Exec['pip_install_django']],
  }

  tails::pip_package_from_repo { 'django-compressor':
    version => '2.4',  # >=2.1.1
    url     => 'https://github.com/django-compressor/django-compressor',
    require => [Exec['pip_install_django-appconf'], Exec['pip_install_rjsmin'], Exec['pip_install_six']],
  }

  tails::pip_package_from_repo { 'django-crispy-forms':
    version => '1.9.1',  # >=1.6.1
    url     => 'https://github.com/django-crispy-forms/django-crispy-forms',
  }

  tails::pip_package_from_repo { 'djangorestframework':
    version => '3.11.0',  # >=3.8
    url     => 'https://github.com/encode/django-rest-framework',
    require => [Exec['pip_install_django']],
  }

  tails::pip_package_from_repo { 'filelock':
    version => '3.0.12',  # >=3.0.1
    tag     => 'v3.0.12',
    url     => 'https://github.com/benediktschmitt/py-filelock',
  }

  tails::pip_package_from_repo { 'future':
    version => '0.18.2',  # >=0.18.0
    tag     => 'v0.18.2',
    url     => 'https://github.com/PythonCharmers/python-future',
  }

  tails::pip_package_from_repo { 'importlib-metadata':
    version => '1.6.1',  # >=0.18
    tag     => 'v1.6.1',
    url     => 'https://gitlab.com/python-devs/importlib_metadata',
    require => [Exec['pip_install_zipp']],
  }

  tails::pip_package_from_repo { 'jaraco.functools':
    version => '3.0.1',
    tag     => 'v3.0.1',
    url     => 'https://github.com/jaraco/jaraco.functools',
    require => [Exec['pip_install_more-itertools']],
  }

  tails::pip_package_from_repo { 'jellyfish':
    version => '0.8.2',  # >=0.6.1
    url     => 'https://github.com/jamesturk/jellyfish',
  }

  tails::pip_package_from_repo { 'kombu':
    version => '4.6.10',  # >=4.6.10, <4.7
    tag     => 'v4.6.10',
    url     => 'http://github.com/celery/kombu',
    require => [Exec['pip_install_amqp'], Exec['pip_install_importlib-metadata'], Exec['pip_install_redis']],
  }

  tails::pip_package_from_repo { 'more-itertools':
    version => '8.4.0',  # >=2.6
    tag     => 'v8.4.0',
    url     => 'https://github.com/more-itertools/more-itertools',
  }

  tails::pip_package_from_repo { 'oauthlib':
    version => '3.1.0',  # >=3.0.0
    tag     => 'v3.1.0',
    url     => 'https://github.com/oauthlib/oauthlib',
  }

  tails::pip_package_from_repo { 'openpyxl':
    version   => '2.6.4',  # >=2.5.0, <3.0
    url       => 'https://bitbucket.org/openpyxl/openpyxl/src/default',
    repo_type => 'hg',
  }

  tails::pip_package_from_repo { 'python3-openid':
    version => '3.1.0',  # >=3.0.10
    tag     => 'v3.1.0',
    url     => 'http://github.com/necaris/python3-openid',
  }

  tails::pip_package_from_repo { 'redis':
    version => '3.5.3',  # >=3.2.0
    url     => 'https://github.com/andymccurdy/redis-py',
  }

  tails::pip_package_from_repo { 'rjsmin':
    version => '1.1.0',  # ==1.1.0
    url     => 'https://github.com/ndparker/rjsmin',
  }

  tails::pip_package_from_repo { 'siphashc':
    version => '1.3',  # >=0.8
    tag     => 'v1.3',
    url     => 'https://github.com/WeblateOrg/siphashc',
  }

  tails::pip_package_from_repo { 'six':
    version => '1.15.0',  # >=1.7.0, >=1.11.0
    url     => 'https://github.com/benjaminp/six',
  }

  tails::pip_package_from_repo { 'social-auth-app-django':
    version => '3.4.0',  # >=3.1.0
    url     => 'https://github.com/python-social-auth/social-app-django',
    require => [Exec['pip_install_six'], Exec['pip_install_social-auth-core']],
  }

  tails::pip_package_from_repo { 'social-auth-core':
    version => '3.3.3',  # >=3.1.0
    url     => 'https://github.com/python-social-auth/social-core',
    require => [Exec['pip_install_oauthlib'], Exec['pip_install_python3-openid'], Exec['pip_install_six']],
  }

  tails::pip_package_from_repo { 'translate-toolkit':
    version => '2.5.1',  # >=2.3.1
    url     => 'https://github.com/translate/translate',
  }

  tails::pip_package_from_repo { 'translation-finder':
    version => '2.1',  # >=1.0
    url     => 'https://github.com/WeblateOrg/translation-finder.git',
  }

  tails::pip_package_from_repo { 'ua-parser':
    version => '0.10.0',  # >=0.9.0
    url     => 'https://github.com/ua-parser/uap-python',
  }

  tails::pip_package_from_repo { 'user-agents':
    version => '2.1',  # >=1.1.0
    tag     => 'v2.1.0',
    url     => 'https://github.com/selwin/python-user-agents',
    require => [Exec['pip_install_ua-parser']],
  }

  tails::pip_package_from_repo { 'vine':
    version => '1.3.0',  # ==1.3.0
    tag     => 'v1.3.0',
    url     => 'http://github.com/celery/vine',
  }

  tails::pip_package_from_repo { 'zipp':
    version => '3.1.0',  # >=0.5
    tag     => 'v3.1.0',
    url     => 'https://github.com/jaraco/zipp',
  }

}
