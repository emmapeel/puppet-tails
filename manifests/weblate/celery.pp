# Use Celery as a task queue for Weblate
class tails::weblate::celery (
  Stdlib::Absolutepath $logs_dir,
  Stdlib::Absolutepath $code_git_checkout,
  String $redis_password,
)
{

  class {'tails::weblate::redis':
    password => $redis_password,
  }

  file { '/var/log/celery':
    ensure => directory,
    owner  => weblate,
    group  => weblate,
    mode   => '0755',
  }

  file { '/etc/systemd/system/celery-weblate.service':
    ensure  => present,
    owner   => root,
    group   => root,
    mode    => '0644',
    content => template('tails/weblate/celery-weblate.service.erb'),
  }

  file { '/var/run/celery':
    ensure => directory,
    owner  => weblate,
    group  => weblate_admin,
    mode   => '0755',
  }

  service { 'celery-weblate':
    ensure    => running,
    provider  => systemd,
    require   => [
      File['/var/log/celery'],
      File['/var/run/celery'],
      Service['redis-server'],
    ],
    subscribe => [
      File['/etc/systemd/system/celery-weblate.service'],
      File['/etc/default/celery-weblate']
    ],
  }

  file { '/etc/default/celery-weblate':
    ensure => present,
    owner  => root,
    group  => root,
    mode   => '0644',
    source => 'puppet:///modules/tails/weblate/etc/default/celery-weblate',
    notify => Service['celery-weblate'],
  }

  file { '/etc/logrotate.d/celery-weblate':
    source  => 'puppet:///modules/tails/weblate/logrotate/celery.conf',
    owner   => root,
    group   => root,
    mode    => '0644',
    require => File[$logs_dir],
    notify  => Service['celery-weblate'],
  }

}
