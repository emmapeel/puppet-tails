# Users and groups needed by many Weblate resources
class tails::weblate::users_and_groups(
  Stdlib::Absolutepath $mutable_data_dir,
) {

  user { 'weblate':
    ensure  => present,
    system  => true,
    home    => $mutable_data_dir,
    gid     => 'weblate',
    require => File[$mutable_data_dir],
  }

  group { 'weblate':
    ensure => present,
    system => true,
  }

  group { 'weblate_admin':
    ensure => present,
    system => true,
  }

}
