# Manage a nginx vhost to get Let's Encrypt certificates for our mail server
class tails::mail::webserver (
  Stdlib::Fqdn $public_hostname = 'mail.tails.boum.org',
) {

  nginx::vhostsd { $public_hostname:
    content => template('tails/nginx/reverse_proxy.vhost.erb'),
    require => [
      Package[nginx],
      File['/etc/nginx/include.d/site_ssl.conf'],
      Tails::Letsencrypt::Certonly[$public_hostname],
    ];
  }

  tails::letsencrypt::certonly { $public_hostname: }

  # Export Let's Encrypt file resources that tails::schleuder will collect
  @@file { "/etc/ssl/${public_hostname}/cert.pem":
    tag     => 'tails_mail_tls',
    content => $::tails_mail_tls_cert,
    owner   => 'root',
    group   => 'root',
    mode    => '0400',
  }
  @@file { "/etc/ssl/${public_hostname}/fullchain.pem":
    tag     => 'tails_mail_tls',
    content => $::tails_mail_tls_fullchain,
    owner   => 'root',
    group   => 'root',
    mode    => '0400',
  }
  @@file { "/etc/ssl/${public_hostname}/privkey.pem":
    tag     => 'tails_mail_tls',
    content => $::tails_mail_tls_privkey,
    owner   => 'root',
    group   => 'root',
    mode    => '0400',
  }
}
