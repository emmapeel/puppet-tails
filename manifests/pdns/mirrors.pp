class tails::pdns::mirrors (
  Array[String] $teammembers,
  Hash $mirrorteam_sshkeys,
) {

  include tails::pdns

  ensure_packages(['python-requests','git'])

  user { 'tails_git':
    ensure         => present,
    system         => true,
    shell          => '/usr/bin/git-shell',
    home           => '/srv/repositories',
    purge_ssh_keys => true,
  }

  file { '/srv/repositories':
    ensure  => directory,
    owner   => 'tails_git',
    mode    => '2700',
    require => User['tails_git'],
  }

  $teammembers.each |String $name| {
    ssh_authorized_key { "${name}-to-tails_git":
      ensure  => present,
      user    => 'tails_git',
      type    => $mirrorteam_sshkeys[$name]['type'],
      key     => $mirrorteam_sshkeys[$name][key],
      require => File['/srv/repositories'],
    }
  }

  vcsrepo { '/srv/repositories/mirrors.git':
    ensure   => bare,
    provider => 'git',
    user     => 'tails_git',
    require  => File['/srv/repositories'],
  }

  file { '/srv/repositories/mirrors.git/hooks/post-update':
    source  => 'puppet:///modules/tails/pdns/mirrors-post-update.hook',
    mode    => '0700',
    owner   => 'tails_git',
    require => [
      Vcsrepo['/srv/repositories/mirrors.git'],
      File['/usr/local/bin/update-dl.py'],
    ],
  }

  file { '/usr/local/bin/update-dl.py':
    owner   => 'tails_git',
    mode    => '0700',
    source  => 'puppet:///modules/tails/pdns/update-dl.py',
    require => [
      User['tails_git'],
      File['/etc/pdnsapi.secret'],
    ],
  }

  file { '/etc/pdnsapi.secret':
    owner   => 'tails_git',
    mode    => '0600',
    content => $tails::pdns::pdns_api_key,
    require => User['tails_git'],
  }

}
