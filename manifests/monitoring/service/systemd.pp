# Manages the monitoring of systemd.
define tails::monitoring::service::systemd (
  String $zone,
  String $nodename,
  Enum['present', 'absent'] $ensure = 'present',
  Boolean $enable_notifications     = true,
  String $check_interval            = '1h',
  String $retry_interval            = '10m',
  Optional[String] $display_name    = undef,
){

  if $display_name == undef {
    $displayed_name = $name
  } else {
    $displayed_name = $display_name
  }

  include ::tails::monitoring::checkcommand::systemd

  file { "/etc/icinga2/zones.d/${zone}/systemd.conf":
    ensure  => $ensure,
    owner   => 'nagios',
    group   => 'nagios',
    mode    => '0600',
    content => template('tails/monitoring/service/systemd.erb'),
    require => Class['::tails::monitoring::checkcommand::systemd'],
    notify  => Service['icinga2'],
  }

}
