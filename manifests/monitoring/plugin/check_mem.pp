class tails::monitoring::plugin::check_mem (
  Enum['present', 'absent'] $ensure  = 'present',
){

  file {'/usr/lib/nagios/plugins/check_mem.pl':
    ensure => $ensure,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
    source => 'puppet:///modules/tails/monitoring/icinga2/plugins/check_mem.pl',
  }

  file_line {'enable_plugins_contrib':
    ensure  => $ensure,
    path    => '/etc/icinga2/icinga2.conf',
    line    => 'include <plugins-contrib>',
    require => File['/usr/lib/nagios/plugins/check_mem.pl'],
    notify  => Service['icinga2'],
  }

}
