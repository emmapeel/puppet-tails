class tails::monitoring::plugin::check_number_in_file (
  Enum['present', 'absent'] $ensure  = 'present',
){

  file {'/usr/lib/nagios/plugins/check_number_in_file':
    ensure => $ensure,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
    source => 'puppet:///modules/tails/monitoring/icinga2/plugins/check_number_in_file',
    notify => Service['icinga2'],
  }

}
