class tails::monitoring::plugin::check_rsync (
  Enum['present', 'absent'] $ensure  = 'present',
){

  file {'/usr/lib/nagios/plugins/check_rsync':
    ensure => $ensure,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
    source => 'puppet:///modules/tails/monitoring/icinga2/plugins/check_rsync',
    notify => Service['icinga2'],
  }

}
