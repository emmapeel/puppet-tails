# Manage a zone in the Tails icinga2 network.

define tails::monitoring::config::zone (
  Variant[Integer, String] $order,
  Optional[String] $endpoint        = undef,
  Optional[String] $parent          = undef,
  Enum['present', 'absent'] $ensure = 'present',
){

  $directory_ensure = $ensure ? {
    absent  => absent,
    default => directory,
  }

  $directory_force = $ensure ? {
    absent  => true,
    default => false,
  }

  file { "/etc/icinga2/zones.d/${name}":
    ensure  => $directory_ensure,
    owner   => 'nagios',
    group   => 'nagios',
    mode    => '0700',
    require => Package['icinga2-common'],
    force   => $directory_force,
  }

  concat::fragment { "zone_${name}":
    target  => '/etc/icinga2/zones.conf',
    order   => $order,
    content => template('tails/monitoring/zone.conf.erb'),
  }

}
