# Manage the installation of ido database feature.
class tails::monitoring::config::ido (
  String $ido_db_name,
  String $ido_db_user,
  String $ido_db_pass,
  String $db_admin_pass,
  Enum['present', 'absent'] $ensure = present,
){

  $link_ensure = $ensure ? {
    absent  => absent,
    default => link,
  }

  package { 'dbconfig-common':
    ensure => $ensure,
  }

  file { '/etc/dbconfig-common/icinga2-ido-mysql.conf':
    ensure  => $ensure,
    owner   => 'root',
    group   => 'root',
    mode    => '0600',
    content => template('tails/monitoring/icingaweb2/dbconfig-common/icinga2-ido-mysql.conf.erb'),
    require => Package['dbconfig-common'],
  }

  package { 'icinga2-ido-mysql':
    ensure  => $ensure,
    require => [
      File['/etc/dbconfig-common/icinga2-ido-mysql.conf'],
    ],
  }

  file { '/etc/icinga2/features-enabled/ido-mysql.conf':
    ensure  => $link_ensure,
    owner   => 'nagios',
    group   => 'nagios',
    mode    => '0644',
    target  => '/etc/icinga2/features-available/ido-mysql.conf',
    require => [
      Package['icinga2-common'],
      Package['icinga2-ido-mysql'],
    ],
    notify  => Service['icinga2'],
  }

}
