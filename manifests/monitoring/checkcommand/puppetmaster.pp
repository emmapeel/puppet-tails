class tails::monitoring::checkcommand::puppetmaster (
  Enum['present', 'absent'] $ensure = 'present',
){

  include ::tails::monitoring::plugin::check_puppetmaster

  file { '/etc/icinga2/conf.d/check_puppetmaster.conf':
    ensure  => $ensure,
    owner   => 'nagios',
    group   => 'nagios',
    mode    => '0600',
    source  => 'puppet:///modules/tails/monitoring/icinga2/checkcommands/check_puppetmaster.conf',
    require => Class['tails::monitoring::plugin::check_puppetmaster'],
    notify  => Service['icinga2'],
  }

}
