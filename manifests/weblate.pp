# Manage a Weblate service
class tails::weblate (
  Array[String] $admins,
  String $db_password,
  String $weblate_secret_key,
  String $redis_password,
  String $db_user                         = 'weblate',
  String $db_name                         = 'weblate',
  String $code_git_remote                 = 'https://github.com/WeblateOrg/weblate.git',
  String $code_git_revision               = 'weblate-3.5.1',
  Stdlib::Absolutepath $apache_data_dir   = '/var/www/weblate',
  Stdlib::Absolutepath $tmserver_data_dir = '/var/lib/tmserver',
) inherits tails::website::params {

  ### Constants
  #
  # Their value is hard-coded in scripts that are hard to maintain
  # as templates, which is why they are not class parameters.
  # Passing these values as arguments to the aforementioned scripts
  # would be doable but require considerable effort for little value.

  $code_git_checkout = '/usr/local/share/weblate'
  $logs_dir          = '/var/log/weblate'
  $mutable_data_dir  = '/var/lib/weblate'

  ### Sanity checks

  if $::lsbdistcodename != 'stretch' {
    fail('The tails::weblate class only supports Debian Stretch.')
  }

  ### Define a stage to ensure requirements for many other resources

  stage { 'weblate-last': }
  Stage['main'] -> Stage['weblate-last']

  ### Operating system configurations

  class {'tails::weblate::mutable_data_dir':
    mutable_data_dir => $mutable_data_dir,
  }

  class {'tails::weblate::users_and_groups':
    mutable_data_dir => $mutable_data_dir,
  }


  class {'tails::weblate::service_admins':
    service_admins => $admins,
    stage          => weblate-last,
  }

  include tails::weblate::debian_packages
  include tails::weblate::python_modules


  ### Weblate configuration

  class {'tails::weblate::webserver':
    mutable_data_dir  => $mutable_data_dir,
    apache_data_dir   => $apache_data_dir,
    code_git_checkout => $code_git_checkout,
    stage             => weblate-last,
  }

  class {'tails::weblate::database':
    db_name     => $db_name,
    db_user     => $db_user,
    db_password => $db_password,
  }

  class {'tails::weblate::repositories':
    mutable_data_dir  => $mutable_data_dir,
    code_git_checkout => $code_git_checkout,
    code_git_remote   => $code_git_remote,
    code_git_revision => $code_git_revision,
    stage             => weblate-last,
  }

  class {'tails::weblate::config':
    mutable_data_dir   => $mutable_data_dir,
    apache_data_dir    => $apache_data_dir,
    code_git_checkout  => $code_git_checkout,
    db_name            => $db_name,
    db_user            => $db_user,
    db_password        => $db_password,
    weblate_secret_key => $weblate_secret_key,
    redis_password     => $redis_password,
    stage              => weblate-last,
  }

  class {'tails::weblate::logs':
    mutable_data_dir => $mutable_data_dir,
    logs_dir         => $logs_dir,
    stage            => weblate-last,
  }

  class {'tails::weblate::tmserver':
    mutable_data_dir   => $mutable_data_dir,
    tmserver_data_dir  => $tmserver_data_dir,
    po_slave_languages => $weblate_slave_languages,
    stage              => weblate-last,
  }

  class {'tails::weblate::celery':
    logs_dir          => $logs_dir,
    redis_password    => $redis_password,
    code_git_checkout => $code_git_checkout,
    stage             => weblate-last,
  }

  ### Integration with Tails website

  class {'tails::weblate::scripts':
    mutable_data_dir             => $mutable_data_dir,
    code_git_checkout            => $code_git_checkout,
    logs_dir                     => $logs_dir,
    production_slave_languages   => $production_slave_languages,
    weblate_additional_languages => $weblate_additional_languages,
    stage                        => weblate-last,
  }

  class {'tails::weblate::staging_website':
    mutable_data_dir   => $mutable_data_dir,
    po_slave_languages => $weblate_slave_languages,
    stage              => weblate-last,
  }

}
