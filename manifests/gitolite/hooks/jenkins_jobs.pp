# Manage Git hooks specific to jenkins-jobs.git
class tails::gitolite::hooks::jenkins_jobs {

  # run by post-update -> post_update.d.hook -> post-update.d/*
  file { '/var/lib/gitolite3/repositories/jenkins-jobs.git/hooks/post-update.d/deploy.hook':
    content => template(
      'tails/gitolite/hooks/jenkins_jobs-post-update.hook.erb'
    ),
    mode    => '0700',
    owner   => gitolite3,
    group   => gitolite3,
  }

  file { '/var/lib/gitolite3/repositories/jenkins-jobs.git/hooks/pre-receive':
      content => template(
        'tails/gitolite/hooks/jenkins_jobs-pre-receive.hook.erb'
      ),
      mode    => '0700',
      owner   => gitolite3,
      group   => gitolite3,
  }

}
