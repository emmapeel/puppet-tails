# Manage Git hooks specific to tails.git
class tails::gitolite::hooks::tails () inherits tails::website::params {

  $hooks_directory = '/var/lib/gitolite3/repositories/tails.git/hooks'

  file { $hooks_directory:
    ensure => directory,
    mode   => '0755',
    owner  => gitolite3,
    group  => gitolite3,
  }

  file { "${hooks_directory}/post-receive":
    content => template('tails/gitolite/hooks/tails-post-receive.erb'),
    owner   => root,
    group   => root,
    mode    => '0755',
    require => Package[curl],
  }

  ensure_packages(['curl'])

}
