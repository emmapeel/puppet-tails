# Manage APT on Tails systems
class tails::apt (
  Enum['update', 'unattended_upgrades'] $cron_mode,
  String $codename                  = 'stretch',
  String $repos                     = 'main',
  String $debian_url                = 'http://ftp.us.debian.org/debian',
  Boolean $include_src              = false,
  Boolean      $proxy               = true,
  String       $proxy_host          = "apt-proxy.${::domain}",
  Stdlib::Port $proxy_port          = 3142,
  Enum['news', 'changelog', 'both']
    $listchanges_which              = 'news',
  Optional[String] $email_recipient = 'tails-sysadmins@boum.org',
  Optional[String] $custom_keys     = 'puppet:///modules/tails/apt/keys.d',
) {

  $proxy_hash = $proxy ? {
    true => {
      host => $proxy_host,
      port => $proxy_port,
    },
    false => {},
  }

  class { 'apt':
    proxy            => $proxy_hash,
    include_defaults => {
      'deb' => true,
      'src' => $include_src,
    },
    purge            => {
      'sources.list'   => true,
      'sources.list.d' => true,
      'preferences'    => true,
      'preferences.d'  => true,
    },
  }

  apt::source { $codename:
    location => $debian_url,
    release  => $codename,
    repos    => $repos,
    pin      => {
      originator => 'Debian',
      codename   => $codename,
      priority   => 990,
    },
  }

  case $codename {
    'stretch': {
      apt::source { "${codename}-updates":
        location => $debian_url,
        release  => "${codename}-updates",
        repos    => $repos,
        pin      => {
          originator => 'Debian',
          codename   => "${codename}-updates",
          priority   => 990,
        },
      }
      apt::source { "${codename}-security":
        location => 'http://security.debian.org/debian-security',
        release  => "${codename}/updates",
        repos    => $repos,
      }
      class { 'apt::backports':
        pin      => 100,
        location => $debian_url,
      }
      apt::source { 'buster':
        location => $debian_url,
        release  => 'buster',
        repos    => $repos,
        pin      => {
          originator => 'Debian',
          codename   => 'buster',
          priority   => 2,
        },
      }
      apt::source { 'buster-updates':
        location => $debian_url,
        release  => 'buster-updates',
        repos    => $repos,
        pin      => {
          originator => 'Debian',
          codename   => 'buster-updates',
          priority   => 2,
        },
      }
      apt::source { 'buster-security':
        location => 'http://security.debian.org/debian-security',
        release  => 'buster/updates',
        repos    => $repos,
      }
      apt::source { 'testing':
        location => $debian_url,
        release  => 'testing',
        repos    => $repos,
        pin      => {
          originator => 'Debian',
          release    => 'testing',
          priority   => 2,
        },
      }
    }
    'buster': {
      apt::source { "${codename}-updates":
        location => $debian_url,
        release  => "${codename}-updates",
        repos    => $repos,
        pin      => {
          originator => 'Debian',
          codename   => "${codename}-updates",
          priority   => 990,
        },
      }
      apt::source { "${codename}-security":
        location => 'http://security.debian.org/debian-security',
        release  => "${codename}/updates",
        repos    => $repos,
      }
      class { 'apt::backports':
        pin      => 100,
        location => $debian_url,
      }
      apt::source { 'bullseye':
        location => $debian_url,
        release  => 'bullseye',
        repos    => $repos,
        pin      => {
          originator => 'Debian',
          codename   => 'bullseye',
          priority   => 2,
        },
      }
      apt::source { 'bullseye-updates':
        location => $debian_url,
        release  => 'bullseye-updates',
        repos    => $repos,
        pin      => {
          originator => 'Debian',
          codename   => 'bullseye-updates',
          priority   => 2,
        },
      }
      #apt::source { 'bullseye-security':
      #  location => 'http://security.debian.org/debian-security',
      #  release  => 'bullseye/updates',
      #  repos    => $repos,
      #}
    }
    'sid': {
      apt::source { 'experimental':
        location => $debian_url,
        repos    => 'main contrib non-free',
        release  => 'experimental',
        pin      => {
          originator => 'Debian',
          release    => 'experimental',
          priority   => 2,
        },
      }
    }
    default: {
      fail("codename = ${codename} is not supported by tails::apt yet.")
    }
  }

  apt::pin { 'zzz_Debian_fallback':
    originator => 'Debian',
    priority   => -10,
  }

  case $cron_mode {
    'update':           {
      ::apt::conf { 'periodic_enable':
        content  => 'APT::Periodic::Enable "1";',
        priority => 11,
      }
      ::apt::conf { 'periodic_download_upgradeable_packages':
        content  => 'APT::Periodic::Download-Upgradeable-Packages "1";',
        priority => 11,
      }
      cron { 'apt_cron_every_N_hours': ensure => absent }
    }
    'unattended_upgrades': {
      class { 'unattended_upgrades':
        mail    => { 'to' => $email_recipient, },
        origins => [ 'origin=*', ],
      }
    }
    default: {}
  }

  # Run apt-daily-upgrade.service 6 times a day: apt-daily.timer
  # updates APT lists up to twice a day which apt-daily-upgrade.timer
  # triggers upgrades only once a day. Between the APT lists update
  # and the upgrade, our monitoring gets unhappy. We used to apply
  # upgrades 6 times a day with cron-apt so let's do the same.
  # Note that this will only apply upgrades if the relevant
  # APT::Periodic::* options are set, which we do above depending on
  # $cron_mode, so what follows is safe even on systems where we
  # disable unattended upgrades.
  service { 'apt-daily-upgrade.timer': ensure => running }
  file { '/etc/systemd/system/apt-daily-upgrade.timer.d':
    ensure => directory,
    owner  => root,
    group  => root,
    mode   => '0755',
  }
  file { '/etc/systemd/system/apt-daily-upgrade.timer.d/frequency.conf':
    owner   => root,
    group   => root,
    mode    => '0644',
    content => "[Timer]
# Reset the default value
OnCalendar=
# Run apt-daily-upgrade.service every 4 hours
OnCalendar=*-*-* 0,4,8,12,16,20:00
",
    notify  => Service['apt-daily-upgrade.timer'],
  }
  file { '/etc/systemd/system/apt-daily-upgrade.service.d':
    ensure => directory,
    owner  => root,
    group  => root,
    mode   => '0755',
  }
  file { '/etc/systemd/system/apt-daily-upgrade.service.d/frequency.conf':
    owner   => root,
    group   => root,
    mode    => '0644',
    content => "[Service]
# Force upgrading even if we already did so less than 24h ago.
# /usr/lib/apt/apt.systemd.daily uses this timestamp file
# to tell when it applied upgrades last, and would otherwise
# refuse applying them if it already did so in the last 24h.
ExecStartPre=/bin/rm -f /var/lib/apt/periodic/upgrade-stamp
",
  }

  class { 'apt_listchanges': which => $listchanges_which }

  # Ensure we are told by our monitoring system when reboot-notifier
  # creates its flag file, but disable reboot-notifier's own email
  # notification system instead.
  package { 'reboot-notifier': ensure => installed }
  file_line { 'disable_reboot-notifier_email':
    path    => '/etc/default/reboot-notifier',
    match   => 'NOTIFICATION_EMAIL=.*',
    line    => 'NOTIFICATION_EMAIL=',
    require => Package['reboot-notifier'],
  }

  ::apt::conf { 'periodic_autoclean_interval':
    content  => 'APT::Periodic::AutocleanInterval "1";',
    priority => '08',
  }
  ::apt::conf { 'periodic_clean_interval':
    content  => 'APT::Periodic::CleanInterval "1";',
    priority => '09',
  }
  ::apt::conf { 'dont_keep_downloaded_packages':
    content  => 'APT::Keep-Downloaded-Packages "false";',
    priority => 10,
  }
  ::apt::conf { 'proxy_bugs_debian_org':
    content  => 'Acquire::HTTP::Proxy::bugs.debian.org "DIRECT";',
  }

  if $custom_keys {
    $keys_directory = '/var/lib/apt/puppet-keys.d'
    file { $keys_directory:
      ensure  => directory,
      source  => $custom_keys,
      recurse => true,
      owner   => root,
      group   => root,
      mode    => '0755',
    }
    exec { 'custom_keys':
      command     => "find '${keys_directory}' -type f -exec apt-key add '{}' \\;",
      subscribe   => File[$keys_directory],
      refreshonly => true,
      notify      => Class['apt::update'],
    }
  }

}
