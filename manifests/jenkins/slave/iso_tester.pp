# Manage a Jenkins slave ready to run the Tails ISO test suite.
class tails::jenkins::slave::iso_tester (
  Enum['present', 'absent'] $ensure      = 'present',
  Stdlib::Absolutepath $config_basedir   = '/etc/TailsToaster',
  Boolean $manage_temp_dir_mount         = false,
  String $root_ssh_pubkey_name           = "root@${::fqdn}",
  String $jenkins_master_ssh_pubkey_name = "jenkins@jenkins-master.${::domain}",
  Stdlib::Absolutepath $temp_dir         = '/tmp/TailsToaster',
  $temp_dir_backing_device               = undef,
  String $temp_dir_fs_type               = 'ext4',
  String $temp_dir_mount_options         = 'relatime,acl',
  String $test_suite_shared_secrets_repo = 'git@gitlab-ssh.tails.boum.org:tails/test-suite-shared-secrets.git',
  Boolean $manage_email_server           = true,
) {

  realize User['jenkins']

  include ::tails::jenkins::slave
  include ::tails::tester::check_po

  class { '::tails::tester':
    manage_temp_dir_mount   => $manage_temp_dir_mount,
    temp_dir_backing_device => $temp_dir_backing_device,
    temp_dir                => $temp_dir,
    temp_dir_fs_type        => $temp_dir_fs_type,
    temp_dir_mount_options  => $temp_dir_mount_options,
  }

  if $manage_email_server {
    include tails::tester::support::email
  }

  file {
    $config_basedir:
      ensure => directory,
      mode   => '0755',
      owner  => 'root',
      group  => 'root';
    "${config_basedir}/local.d":
      source  => "puppet:///modules/tails_secrets_jenkins/jenkins/slaves/${::fqdn}/TailsToaster_config",
      owner   => 'jenkins',
      group   => 'jenkins',
      purge   => true,
      recurse => true;
  }

  sshkeys::set_client_key_pair { $root_ssh_pubkey_name:
    keyname => $root_ssh_pubkey_name,
    user    => 'root',
    home    => '/root',
  }

  vcsrepo { "${config_basedir}/common.d":
    ensure   => latest,
    owner    => jenkins,
    group    => jenkins,
    user     => root,
    provider => git,
    source   => $test_suite_shared_secrets_repo,
    require  => [
      Sshkeys::Set_client_key_pair[$root_ssh_pubkey_name],
      Package['git'],
      File[$config_basedir],
    ],
  }

  file { '/usr/local/bin/wrap_test_suite':
    ensure => $ensure,
    source => 'puppet:///modules/tails/jenkins/slaves/isotesters/wrap_test_suite',
    mode   => '0755',
    owner  => 'root',
    group  => 'root',
  }

  file { '/usr/local/bin/post_test_cleanup':
    ensure => $ensure,
    source => 'puppet:///modules/tails/jenkins/slaves/isotesters/post_test_cleanup',
    mode   => '0755',
    owner  => 'root',
    group  => 'root',
  }

  # Wait for a shorter duration than the default 300s before forcibly
  # shutting down a leftover TailsToaster guest on poweroff/reboot.
  # This makes the tester VM available again faster after
  # a test_Tails_ISO_* job has been canceled.
  augeas { 'libvirt-guests-SHUTDOWN_TIMEOUT':
    context => '/files/etc/default/libvirt-guests',
    changes => 'set SHUTDOWN_TIMEOUT 30',
    require => Class['::tails::tester'],
  }

  # Prevent some Recommends pulled by GTK3 to be installed.
  # Some of them imply running daemons we don't need, which slows down the boot
  # ... and we're going to reboot these VMs quite often.
  package { ['colord', 'libsane', 'sane-utils']:
    ensure => absent,
  }

  sshkeys::set_authorized_keys{ 'jenkins_master_to_iso_tester':
    keyname => $jenkins_master_ssh_pubkey_name,
    user    => 'jenkins',
    home    => '/var/lib/jenkins',
  }

}
