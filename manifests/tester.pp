# Set up what's necessary to run automated tests
class tails::tester (
  Boolean $manage_temp_dir_mount            = false,
  String $temp_dir                          = '/tmp/TailsToaster',
  Optional[String] $temp_dir_backing_device = undef,
  String $temp_dir_fs_type                  = 'ext4',
  String $temp_dir_mount_options            = 'relatime,acl',
) {

  ### Sanity checks

  if $::lsbdistcodename !~ /^stretch|buster$/ {
    warning('The tails::tester class only supports Debian Stretch and Buster.')
  }

  ### Resources

  $stretch_and_older_only_ensure = $::lsbdistcodename ? {
    stretch => present,
    default => absent,
  }

  $stretch_and_older_packages = [
    'libvpx4',
  ]

  $python_opencv_packages = $::lsbdistcodename ? {
    stretch => ['python-opencv', 'python-pil'],
    default => ['python3-opencv', 'python3-pil'],
  }

  class { '::libvirt::host':
    qemu_security_driver => 'none',
  }

  # Stretch: workaround #12142
  apt::source { 'isotester-stretch':
    ensure   => $stretch_and_older_only_ensure,
    location => 'http://deb.tails.boum.org/',
    release  => 'isotester-stretch',
    repos    => 'main',
  }
  apt::pin { 'qemu':
    ensure   => $stretch_and_older_only_ensure,
    packages => ['qemu*'],
    origin   => 'deb.tails.boum.org',
    priority => 991,
  }

  apt::pin { 'tor':
    ensure     => $stretch_and_older_only_ensure,
    packages   => ['tor', 'tor-geoipdb'],
    originator => 'Debian Backports',
    codename   => 'stretch-backports',
    priority   => 991,
  }

  $tester_packages = [
    'cucumber',
    'devscripts',
    'dnsmasq-base',
    'gawk',
    'git',
    'i18nspector',
    'imagemagick',
    'libav-tools',
    'libcap2-bin',
    'libvirt0',
    'libvirt-clients',
    'libvirt-daemon-system',
    'libvirt-dev',
    'obfs4proxy',
    'ovmf',
    'pry',
    'python-jabberbot',
    'python-potr',
    *$python_opencv_packages,
    'qemu-kvm',
    'qemu-system-x86',
    'redir',
    'ruby-guestfs',
    'ruby-json',
    'ruby-libvirt',
    'ruby-net-irc',
    'ruby-packetfu',
    'ruby-rb-inotify',
    'ruby-rjb',
    'ruby-rspec',
    'ruby-test-unit',
    'seabios',
    'tcpdump',
    'tcplay',
    'unclutter',
    'virt-viewer',
    'x11vnc',
    'x264',
    'xdotool',
    'xtightvncviewer',
    'xvfb',
  ]

  ensure_packages($tester_packages)

  package { $stretch_and_older_packages:
    ensure => $stretch_and_older_only_ensure,
  }

  file { '/etc/tmpfiles.d/TailsToaster.conf':
    ensure  => file,
    owner   => root,
    group   => root,
    mode    => '0644',
    content => "d  ${temp_dir}  0755  root  root  -\n",
  }

  if $manage_temp_dir_mount {
    validate_string(
      $temp_dir_backing_device,
      $temp_dir_fs_type,
      $temp_dir_mount_options
    )

    mount { $temp_dir:
      ensure  => mounted,
      device  => $temp_dir_backing_device,
      fstype  => $temp_dir_fs_type,
      options => $temp_dir_mount_options,
      require => File['/etc/tmpfiles.d/TailsToaster.conf']
    }

  }
}
