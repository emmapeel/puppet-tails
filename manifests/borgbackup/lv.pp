# set up backups for a specific logical volume
#
# note that after setting up a backup job via puppet, you have to manually copy
# the keyfiles from /root/.config/borg/keys on the host that runs the backups
# to the system from which you want to be able to do recoveries. these keyfiles
# together with the borg passphrase are a hard requirement for any recovery.

define tails::borgbackup::lv (
  Stdlib::Host $backupserver = 'stone.tails.boum.org',
  Stdlib::Unixpath $basedir  = '/srv/backups',
  String $backupuser         = 'borg',
  String $vg                 = 'lizard',
  String $repo               = $title,
  String $lv                 = $title,
  Array[String] $excludes    = [],
  Boolean $rawdisk           = false,
) {

  include tails::borgbackup

  file { "/etc/tails-borgbackup/${lv}.excludes":
    content => template('tails/borgbackup/excludes.erb'),
    mode    => '0600',
    owner   => 'root',
    group   => 'root',
  }

  $runbackup_options = $rawdisk ? { true => '-d', default => '' }
  cron { "runbackup ${lv}":
    command => "/usr/bin/chronic /usr/local/sbin/runbackuplv.sh -v '${vg}' -l '${lv}' -u '${backupuser}' -s '${backupserver}' -b '${basedir}' -r '${repo}' ${runbackup_options}",
    user    => root,
    weekday => fqdn_rand(7, $title),
    hour    => fqdn_rand(24, $title),
    minute  => fqdn_rand(60, $title),
    require => [
      Class['tails::borgbackup'],
      File["/etc/tails-borgbackup/${lv}.excludes"],
    ],
  }

  exec { "/usr/bin/borg init --encryption=keyfile --append-only '${backupuser}@${backupserver}:${basedir}/${repo}'":
    onlyif      => "/usr/bin/borg list '${backupuser}@${backupserver}:${basedir}/${repo}' 2>&1 |grep -qs 'does not exist'",
    environment => "BORG_PASSPHRASE=${tails::borgbackup::borgpassphrase}",
    require     => Class['tails::borgbackup'],
  }
}
