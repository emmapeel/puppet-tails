# Manage what's needed to serve an instance of the Tails website
#
# Dependencies: nginx module and class already included
#
# Know caveats: "ensure => absent" cleans up only partially.

define tails::website::webserver::instance (
  Enum['present', 'absent'] $ensure = 'present',
  Stdlib::Fqdn $public_hostname     = $name,
  Stdlib::Port $port                = 443,
  Boolean $cgi                      = true,
  Boolean $ssl                      = true,
  Stdlib::Absolutepath $web_dir     = "/srv/${public_hostname}/html",
  Boolean $stats                    = true,
) {

  ### Sanity checks

  if $ssl and $port == 80 {
    fail('Using port 80 with SSL enabled conflicts with Let\'s Encrypt.')
  }

  ### Resources

  include tails::website::webserver::common

  $log_dir = "/var/log/nginx/${public_hostname}"

  nginx::vhostsd { $public_hostname:
    ensure  => $ensure,
    content => template('tails/website/nginx/site.erb'),
    require => [
      Package[nginx, 'libnginx-mod-http-fancyindex'],
      File[$web_dir],
      Nginx::Included['tails_website_rewrite_rules'],
      Tails::Dhparam['/etc/nginx/dhparams.pem'],
      File[$log_dir],
    ],
  }

  file { $log_dir:
    ensure => directory,
    owner  => 'www-data',
    group  => 'adm',
    mode   => '0750',
  }

  file { "/etc/logrotate.d/nginx-${public_hostname}":
    ensure  => $ensure,
    content => template('tails/website/nginx/logrotate.conf.erb'),
    owner   => root,
    group   => root,
    mode    => '0644',
    require => File[$log_dir],
  }

  if $ssl {
    tails::letsencrypt::certonly { $public_hostname: }
  }

  $cron_ensure = $stats ? { true => present, default => absent }
  cron { "tails-website-email-last-month-stats ${public_hostname}":
    ensure      => $cron_ensure,
    user        => 'www-data',
    monthday    => '2',
    hour        => '17',
    minute      => '57',
    command     => "/usr/local/bin/tails-website-last-month-stats '${log_dir}'",
    environment => [ 'MAILTO=tails@boum.org' ],
    require     => File[
      $log_dir,
      '/usr/local/bin/tails-website-last-month-stats',
    ],
  }

}
