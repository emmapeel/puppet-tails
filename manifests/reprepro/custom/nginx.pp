# Manage the nginx vhost for the Tails overlay APT repository
class tails::reprepro::custom::nginx (
  Stdlib::Fqdn $hostname,
  Stdlib::Absolutepath $basedir,
  String $vhost_content,
){

  assert_private()

  ensure_packages(['libnginx-mod-http-fancyindex'])

  nginx::vhostsd { $hostname:
    content => $vhost_content,
    require => Package[nginx, 'libnginx-mod-http-fancyindex'],
  }

  ensure_packages([acl])

  $acls = "${common::moduledir::module_dir_path}/tails/reprepro-custom-webdirs.acl"

  file { $acls:
    content => "group::r-X\ndefault:group::r-X\nother::r-X\ndefault:other::r-X\n",
    owner   => root,
    group   => root,
    mode    => '0600',
  }

  exec { 'set-acl-on-tails-reprepro-custom-web-dirs':
    command     => "setfacl -R -M '${acls}' '${basedir}/dists' '${basedir}/pool'",
    require     => [
      File[$acls, "${basedir}/dists", "${basedir}/pool"],
      Package[acl],
    ],
    subscribe   => File[$acls],
    refreshonly => true;
  }

}
