# Manage access for a release manager to the time-based APT snapshots
#
# Parameters:
#
#  * name: the name of the release manager, as found in the users hash
#    in Hiera
#  * sshkeys: same as tails::reprepro::snapshots::time_based's
#    $release_managers_sshkeys, this contains the keys of _all_
#    release managers
#  * user: the user to which access is granted
#
define tails::reprepro::snapshots::time_based::release_manager (
  Hash $sshkeys,
  String $user,
) {

  assert_private()

  ssh_authorized_key { "${name}-to-${user}":
    ensure => present,
    user   => $user,
    type   => $sshkeys[$name]['type'],
    key    => $sshkeys[$name][key],
  }

}
