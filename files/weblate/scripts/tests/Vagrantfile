# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "generic/debian9"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "data", "/vagrant_data"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  config.vm.provider "libvirt" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = false
  #
  #   # Customize the amount of memory on the VM:
     vb.memory = "2048"
  end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Define a Vagrant Push strategy for pushing to Atlas. Other push strategies
  # such as FTP and Heroku are also available. See the documentation at
  # https://docs.vagrantup.com/v2/push/atlas.html for more information.
  # config.push.define "atlas" do |push|
  #   push.app = "YOUR_ATLAS_USERNAME/YOUR_APPLICATION_NAME"
  # end

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.

  config.vm.provision "file", source: "01proxy", destination: "/tmp/01proxy"
  config.vm.provision "shell", inline: <<-SHELL
      mv /tmp/01proxy /etc/apt/apt.conf.d/
      apt update
      apt install -y dirmngr python3-venv python3-dev python3-nose po4a translate-toolkit python3-git python3-pil python3-coverage python3-wheel
      cd /usr/local/share
      git clone https://github.com/WeblateOrg/weblate.git
      cd weblate
      git checkout weblate-3.5.1
      cp weblate/settings_example.py weblate/settings.py
      sed -i "s#'NAME': 'weblate.db'#'NAME': '/home/vagrant/weblate.db'#" weblate/settings.py
      mkdir data
      chown vagrant: data
  SHELL

  $checkWeblateTag = <<-SCRIPT
      gpg --auto-key-locate wkd --locate-keys michal@cihar.com
      cd /usr/local/share/weblate
      git tag -v weblate-3.5.1
  SCRIPT
  config.vm.provision "shell", inline: $checkWeblateTag, privileged: false

  $script = <<-SCRIPT
      git clone https://gitlab.tails.boum.org/tails/puppet-tails.git
      python3 -m venv --system-site-packages venv
      . venv/bin/activate
      pip3 install -r /usr/local/share/weblate/requirements.txt
      # Version installed on translation platform see manifests/weblate/python_modules.pp
      pip3 install Django==1.11.26
      pip3 install social-auth-app-django==3.1.0
      pip3 install social-auth-core==3.1.0
      # create database
      /usr/local/share/weblate/manage.py migrate
      #create project "tails"
      /usr/local/share/weblate/manage.py shell -c "from weblate.trans.models.project import Project; Project(name='Tails', slug='tails', web='https://tails.boum.org').save()"
      # fill project with data (only de and fr, as this is enough for the tests
      /usr/local/share/weblate/manage.py import_project --language-regex "^(de|fr)$" tails https://git.tails.boum.org/tails.git master "wiki/src/**.*.po"
  SCRIPT

  config.vm.provision "shell", inline: $script, privileged: false

end
