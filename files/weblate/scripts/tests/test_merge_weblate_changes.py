import os
import pathlib
import tempfile
import unittest

import git
import merge_weblate_changes as mwc

from .utils import createRepo

class TestMWC(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.tempdir = tempfile.TemporaryDirectory()
        cls.repopath = pathlib.Path(cls.tempdir.name)
        createRepo(str(cls.repopath))
        cls.repopath = cls.repopath/"repo"
        cls.repo = git.Repo(str(cls.repopath))
        cls.local = cls.repo.refs["master"]
        cls.path = pathlib.Path(os.path.realpath(__file__)).parent

    @classmethod
    def tearDownClass(cls):
        cls.tempdir.cleanup()

    def merge_message(self, lines):
        head = "merge Weblate changes using merge-weblate-changes.\n\n"
        return (head+"\n".join(lines)).strip()

    def test_remote_deleted_nonWikiPo(self):
        """remote deleted a nonWikiPo file, local don't -> reset this change."""
        mwc.main(self.repopath, "master", "remote-1", "remote-delete")
        self.assertNotEqual(self.local.commit, self.repo.commit("remote-1"))
        self.assertNotEqual(self.local.commit, self.repo.commit("remote-delete"))

        self.assertTrue("a.de.po" in self.local.commit.tree)
        self.assertTrue("a.fr.po" in self.local.commit.tree)
        self.assertTrue("a.mdwn" in self.local.commit.tree)

        self.assertEqual(self.local.commit.message, self.merge_message([
            "a.de.po: reset (Weblate is not allowed to delete files).",
            "a.fr.po: reset (Weblate is not allowed to delete files).",
            "a.mdwn: reset (Weblate is not allowed to delete files).",
            ]))

    def test_remote_add_nonWikiPo(self):
        """remote add a nonWikiPo file, local don't -> reset this change."""
        mwc.main(self.repopath, "master", "remote-1", "remote-add")
        self.assertNotEqual(self.local.commit, self.repo.commit("remote-1"))
        self.assertNotEqual(self.local.commit, self.repo.commit("remote-add"))

        self.assertFalse("b.de.po" in self.local.commit.tree)
        self.assertFalse("b.mdwn" in self.local.commit.tree)

        self.assertEqual(self.local.commit.message, self.merge_message([
            "b.de.po: reset (Weblate is not allowed to add files).",
            "b.mdwn: reset (Weblate is not allowed to add files).",
            ]))

    def test_remote_change_nonWikiPo(self):
        """remote change a nonWikiPo file, local don't -> reset this change."""
        mwc.main(self.repopath, "master", "remote-1", "remote-update")
        self.assertNotEqual(self.local.commit, self.repo.commit("remote-1"))
        self.assertNotEqual(self.local.commit, self.repo.commit("remote-update"))

        self.assertEqual((self.repopath/"a.de.po").read_text(), (self.path/"data/a.de.po.base").read_text())
        self.assertEqual(self.local.commit.message, self.merge_message([
            "a.de.po: reset (Weblate is only to touch po files).",
            ]))

    def test_remote_change_nonWikiPo_submodule(self):
        """remote change a submodule, local don't reset this change."""
        mwc.main(self.repopath, "master", "base-1", "submodule-change")
        self.assertNotEqual(self.local.commit, self.repo.commit("base-1"))
        self.assertNotEqual(self.local.commit, self.repo.commit("submodule-change"))

        submodule = self.repo.submodule('subtest')
        self.assertEqual(submodule.hexsha, submodule.module().commit("base-1").hexsha)
        self.assertEqual(self.local.commit.message, self.merge_message([
            "submodules/test: reset (Weblate is only to touch po files).",
            ]))

    def test_remote_delete_wikiPo(self):
        """remote delete a wikiPo file, that still exists locally -> reset this change."""
        mwc.main(self.repopath, "master", "remote-wiki-add", "remote-wiki-delete")
        self.assertNotEqual(self.local.commit, self.repo.commit("remote-wiki-add"))
        self.assertNotEqual(self.local.commit, self.repo.commit("remote-wiki-delete"))

        self.assertTrue("wiki/src/index.de.po" in self.local.commit.tree/"wiki/src")
        self.assertTrue("wiki/src/index.fr.po" in self.local.commit.tree/"wiki/src")
        self.assertTrue("wiki/src/index.mdwn" in self.local.commit.tree/"wiki/src")

        self.assertEqual(self.local.commit.message, self.merge_message([
            "wiki/src/index.de.po: reset (Weblate is not allowed to delete files).",
            "wiki/src/index.fr.po: reset (Weblate is not allowed to delete files).",
            "wiki/src/index.mdwn: reset (Weblate is not allowed to delete files).",
            ]))

    def test_remote_delete_wikiPo_already_deleted(self):
        """remote delete a wikiPo file, that is also deleted locally -> delete this file."""
        mwc.main(self.repopath, "master", "remote-wiki-delete", "remote-wiki-delete2")
        self.assertNotEqual(self.local.commit, self.repo.commit("remote-wiki-delete"))
        self.assertNotEqual(self.local.commit, self.repo.commit("remote-wiki-delete2"))

        self.assertFalse("wiki" in self.local.commit.tree)
        self.assertEqual(self.local.commit.message, self.merge_message([]))

    def test_remote_add_wikiPo(self):
        """remote adds a wikiPo file -> reset this."""
        mwc.main(self.repopath, "master", "remote-1", "remote-wiki-add")
        self.assertNotEqual(self.local.commit, self.repo.commit("remote-1"))
        self.assertNotEqual(self.local.commit, self.repo.commit("remote-wiki-add"))

        self.assertFalse("wiki" in self.local.commit.tree)

        self.assertEqual(self.local.commit.message, self.merge_message([
            "wiki/src/index.de.po: reset (Weblate is not allowed to add files).",
            "wiki/src/index.fr.po: reset (Weblate is not allowed to add files).",
            "wiki/src/index.mdwn: reset (Weblate is not allowed to add files).",
            ]))

    def test_remote_change_wikiPo(self):
        """remote changes a wikiPo file -> apply this change (fast-forward)."""
        mwc.main(self.repopath, "master", "remote-wiki-add", "remote-wiki-update2")
        self.assertEqual(self.local.commit, self.repo.commit("remote-wiki-update2"))

    def test_only_local_has_changes(self):
        """local has commits, that are not available at remote"""
        mwc.main(self.repopath, "master", "remote-wiki-update", "remote-wiki-add")
        self.assertEqual(self.local.commit, self.repo.commit("remote-wiki-update"))

    def test_remote_change_wikiPo_local_too(self):
        """remote and local changes a wikiPo file -> merge wikiPo file."""
        mwc.main(self.repopath, "master", "remote-wiki-update", "remote-wiki-update2")
        self.assertNotEqual(self.local.commit, self.repo.commit("remote-wiki-update"))
        self.assertNotEqual(self.local.commit, self.repo.commit("remote-wiki-update2"))

        self.assertEqual((self.repopath/"wiki/src/index.de.po").read_text(), (self.path/"data/index.de.po.mwc").read_text())

        self.assertEqual(self.local.commit.message, self.merge_message([
            "wiki/src/index.de.po: merging.",
            ]))


if __name__ == '__main__':
        unittest.main()
