#!/bin/sh
#
# a script to update the staging website
# run as user weblate!

set -e
set -u

STAGING_WEB_DIR=/var/www/staging
STAGING_REPO_DIR=/var/lib/weblate/repositories/vcs/staging
SANITY_CHECK_LOGFILE_BASENAME=last-sanity-errors.txt
SANITY_CHECK_LOGFILE="${STAGING_WEB_DIR}/${SANITY_CHECK_LOGFILE_BASENAME}"
IKIWIKI_LOGFILE=/var/log/weblate/update-staging-website_ikiwiki.log

/var/lib/weblate/scripts/save-suggestions.py "$STAGING_REPO_DIR"

cd "$STAGING_REPO_DIR"

# Note: cron ignores the exit code so on errors, we print a message to
# STDERR, which cron will then send over email

./bin/sanity-check-website > "$SANITY_CHECK_LOGFILE" 2>&1 \
    || echo "sanity-check-website failed; for details, see https://staging.tails.boum.org/${SANITY_CHECK_LOGFILE_BASENAME}" >&2

ikiwiki --setup /var/lib/weblate/config/ikiwiki.setup --refresh \
    > "$IKIWIKI_LOGFILE" 2>&1 \
    || ikiwiki --setup /var/lib/weblate/config/ikiwiki.setup --rebuild > "$IKIWIKI_LOGFILE" 2>&1 \
    || echo "ikiwiki failed; for details, see ${IKIWIKI_LOGFILE} on $(hostname -f)" >&2
