#!/usr/bin/python3

import sys
import argparse
import re
import urllib.request
import lxml.html


def report(code, message):
    if code == 0:
        print("OK: " + message)
    elif code == 2:
        print("CRITICAL: %s" % message)
    sys.exit(code)


def get_url(url):
    '''Retrieve a given url.'''
    req = urllib.request.Request(url)
    try:
        with urllib.request.urlopen(req) as response:
            data = lxml.html.fromstring(response.read())
    except (Exception) as e:
        report(2, format(e))
    return data


def get_last_directory(url):
    '''Retrieve the last Tor Browser directory on the website.'''
    raw_data = get_url(url)
    links = raw_data.xpath('//a/@href')
    if len(links) == 0:
        report(2, "Tor Browser releases subdirectories not found")
    if re.compile(r'(\d|-nightly)\/$').search(links[-1]) is None:
        report(
            2,
            "Link is not a Tor Browser release subdirectory: %s" % links[-1]
        )
    return links[-1]


def has_tarball(release):
    '''Check if at least one tarball exists in a given release directory.'''
    data = get_url(args.url + release)
    links = data.xpath('//a/@href')
    for link in links:
        if re.compile(r'^tor-browser-linux64.*\.tar\.xz$').search(link):
            return True
    report(2, "No tarball found.")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-U",
        "--url",
        help="Url of the Tor Browser archive website to check."
    )
    args = parser.parse_args()
    assert re.compile(r'^https\:\/\/[\w\-\.]+\/$').match(args.url)
    last_dir = get_last_directory(args.url)
    if has_tarball(last_dir):
        report(0, "Tarball found.")
